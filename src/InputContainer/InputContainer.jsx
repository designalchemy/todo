import React, { Component } from 'react';
import css from './InputContainer.scss';

class InputContainer extends Component {

    constructor(props) {
        super(props);
        
        this.state = {
            todos: JSON.parse(localStorage.getItem('todos')) || [],
        }

        this.handleAddItem = this.handleAddItem.bind(this);
    }

    handleAddItem() {
        const textBox = document.getElementById('inputBox');
        const dateBox = document.getElementById('dateBox');
        const colorBox = document.getElementById('colorBox');

        if (!textBox.value) { return false; }

        let todos = JSON.parse(localStorage.getItem('todos')) || [];
        const makeKey = todos.length > 0 ? todos.slice(-1)[0].key + 1 : 0;
        let newObj = {
            key: makeKey,
            text: textBox.value,
            date: dateBox.value,
            color: colorBox.value,
            x: '350',
            y: '50',
            done: false
        };

        todos.push(newObj);
        textBox.value = '';
        dateBox.value = '';
        colorBox.value = '';
        localStorage.setItem('todos', JSON.stringify(todos));

        this.props.updateState(todos);
    }

    render() {

        return (
            <div className="input-container">

                <h1>To do list</h1>

                <div className="input-container_holder">

                    <label>Add new to do item</label>

                    <input type="text" id="inputBox" />

                    <label>Set a due date</label>

                    <input type="date" id="dateBox" />

                    <label>Set a color</label>

                    <input type="color" id="colorBox" />

                    <button onClick={this.handleAddItem}>Add Item</button>

                </div>

            </div>
        );
    }

};

export default InputContainer;

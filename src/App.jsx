import React, { Component } from 'react';

import InputContainer from './InputContainer/InputContainer';
import OutputContainer from './OutputContainer/OutputContainer';
import DoneContainer from './DoneContainer/DoneContainer';

import css from './App.scss';

class App extends Component {

    constructor() {
       super();

       this.state = {
        todos: JSON.parse(localStorage.getItem('todos')) || []
       };

       this.deleteMe = this.deleteMe.bind(this);
       this.setItems = this.setItems.bind(this);
       this.moveToDone = this.moveToDone.bind(this);
       this.moveToNotDone = this.moveToNotDone.bind(this);
     };

    setItems(e) {
        this.setState({
            todos: e
        });
    }

    deleteMe(e) {
        const todos = JSON.parse(localStorage.getItem('todos'));

        for (let i = 0; i < todos.length; i++) {
            if (todos[i].key === e) {
                todos.splice(i, 1);
            }
        }

        localStorage.setItem('todos', JSON.stringify(todos));
        this.setState({ todos: todos });
    }

    moveToDone(e) {
        const todos = JSON.parse(localStorage.getItem('todos'));

        for (let i = 0; i < todos.length; i++) {
            if (todos[i].key === e) {
                todos[i].done = true;
            }
        }

        localStorage.setItem('todos', JSON.stringify(todos));
        this.setState({ todos: todos });
    }

    moveToNotDone(e) {
        const todos = JSON.parse(localStorage.getItem('todos'));

        for (let i = 0; i < todos.length; i++) {
            if (todos[i].key === e) {
                todos[i].done = false;
            }
        }

        localStorage.setItem('todos', JSON.stringify(todos));
        this.setState({ todos: todos });
    }

    render() {
        console.log(this.props.all);
        return (
            <div>
                <div className="container">
                    <div className="row">
                        <main className="main-container">
                            <InputContainer updateState={this.setItems} />
                            <OutputContainer 
                                inputFields={this.state.todos} 
                                moveToDone={this.moveToDone} />
                            <DoneContainer 
                                inputFields={this.state.todos} 
                                deleteMe={this.deleteMe} 
                                moveToNotDone={this.moveToNotDone} />
                        </main>
                    </div>
                </div>
            </div>
        );

    }

};

export default App;

import React, { Component } from 'react';
import css from './OutputItem.scss';

class OutputItem extends Component {

    constructor(props) {
        super(props);
        
        this.state = {
            x: this.props.item.x || 0,
            y: this.props.item.y || 0
        }

        this.stopDrag = this.stopDrag.bind(this);
        this.moveToDone = this.moveToDone.bind(this);
    }

    moveToDone() {
        this.props.moveToDone(this.props.item.key);
    }

    handleDrag(e) {
        //console.log(e.clientX, e.clientY);
    }

    stopDrag(e) {
        let target;
        let todos = JSON.parse(localStorage.getItem('todos'));

        const targetId = this.props.item.key;

        for (let i = 0; i < todos.length; i++) {

            if (todos[i].key == targetId) {
                target = todos[i];
            }
        }

        target.x = e.clientX;
        target.y = e.clientY;

        localStorage.setItem('todos', JSON.stringify(todos));

        this.setState({
            x: e.clientX,
            y: e.clientY
        });

    }

    invertColor(hex, bw) {

        if (hex.indexOf('#') === 0) {
            hex = hex.slice(1);
        }
        // convert 3-digit hex to 6-digits.
        if (hex.length === 3) {
            hex = hex[0] + hex[0] + hex[1] + hex[1] + hex[2] + hex[2];
        }
        if (hex.length !== 6) {
            throw new Error('Invalid HEX color.');
        }
        let r = parseInt(hex.slice(0, 2), 16),
            g = parseInt(hex.slice(2, 4), 16),
            b = parseInt(hex.slice(4, 6), 16);
        if (bw) {
            // http://stackoverflow.com/a/3943023/112731
            return (r * 0.299 + g * 0.587 + b * 0.114) > 186
                ? '#000000'
                : '#FFFFFF';
        }
        // invert color components
        r = (255 - r).toString(16);
        g = (255 - g).toString(16);
        b = (255 - b).toString(16);
        // pad each with zeros and return
        return "#" + this.padZero(r) + this.padZero(g) + this.padZero(b);
        
    }

    padZero(str, len) {
        len = len || 2;
        const zeros = new Array(len).join('0');
        return (zeros + str).slice(-len);
    }

    render() {
        if (this.props.item.done) { return false; }

        const pos = Math.max(0, this.state.y - 30);
        const color = this.props.item.color;
        const correctColor = this.invertColor(color, true);

        return (
            <div 
                draggable="true"
                onDrag={this.handleDrag}
                onDragEnd={this.stopDrag}
                className="output-container__item" 
                style={{
                    top: pos + 'px',
                    left: this.state.x + 'px',
                    background: color,
                    color: correctColor
                }}>
                <p>{this.props.item.text}</p>

                {this.props.item.date &&
                    <span>
                        <hr  style={{border: '1px solid' + correctColor}}/>
                        Due: {this.props.item.date}
                    </span>
                }

                <span
                    className="output-container__item-close"
                    onClick={this.moveToDone}
                >X</span>

            </div>
        );

    }
};

export default OutputItem;

import React, { Component } from 'react';
import OutputItem from './OutputItem/OutputItem';

import css from './OutputContainer.scss';

class OutputContainer extends Component {

    constructor(props) {
        super(props);
        
        this.state = {
            todos: this.props.inputFields
        }

        this.moveToDone = this.moveToDone.bind(this);
    }

    moveToDone(e) {
        this.props.moveToDone(e);
    }

    render() {
        const todos = this.props.inputFields;

        if (!todos.length) { return false; }

        return (
            <div className="output-container">

                {todos.map(function(todo) {
                    return <OutputItem key={todo.key} item={todo} moveToDone={this.moveToDone} />
                }.bind(this))}

            </div>
        );
    }

};

export default OutputContainer;

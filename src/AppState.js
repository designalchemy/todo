import { observable, computed } from 'mobx';

class AppState {
	@observable all = JSON.parse(localStorage.getItem('todos')) || [];

	constructor() {

		this.state = {
			all: all
		}

		this.deleteMe = this.deleteMe.bind(this);
		this.moveToDone = this.moveToDone.bind(this);
		this.moveToNotDone = this.moveToNotDone.bind(this);
	}

	@computed deleteMe(e) {
		const todos = JSON.parse(localStorage.getItem('todos'));

		for (let i = 0; i < todos.length; i++) {
			if (todos[i].key === e) {
				todos.splice(i, 1);
			}
		}

		localStorage.setItem('todos', JSON.stringify(todos));
		this.setState({ todos: todos });
	}

	@computed moveToDone(e) {
		const todos = JSON.parse(localStorage.getItem('todos'));

		for (let i = 0; i < todos.length; i++) {
			if (todos[i].key === e) {
				todos[i].done = true;
			}
		}

		localStorage.setItem('todos', JSON.stringify(todos));
		this.setState({ todos: todos });
	}

	@computed moveToNotDone(e) {
		const todos = JSON.parse(localStorage.getItem('todos'));

		for (let i = 0; i < todos.length; i++) {
			if (todos[i].key === e) {
				todos[i].done = false;
			}
		}

		localStorage.setItem('todos', JSON.stringify(todos));
		this.setState({ todos: todos });
	}

	return (
		
	)
}

export default AppState;

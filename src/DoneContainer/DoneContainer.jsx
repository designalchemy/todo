import React, { Component } from 'react';
import DoneItem from './DoneItem/DoneItem'
import css from './DoneContainer.scss';

class DoneContainer extends Component {

    constructor(props) {
        super(props);
        
        this.deleteMe = this.deleteMe.bind(this);
        this.moveToNotDone = this.moveToNotDone.bind(this);
    }

    deleteMe(e) {
        this.props.deleteMe(e);
    }

    moveToNotDone(e) {
        this.props.moveToNotDone(e);
    }

    render() {
        const items = this.props.inputFields;

        return (
            <div className="done-container">

                <h1>Done</h1>

                <div className="done-container_holder">

                    {items.map(function(item) {
                        return <DoneItem 
                                    key={item.key} 
                                    items={item} 
                                    deleteMe={this.deleteMe}
                                    moveToNotDone={this.moveToNotDone} />
                    }.bind(this))}

                </div>

            </div>
        );

    }
};

export default DoneContainer;

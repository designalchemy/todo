import React, { Component } from 'react';
import css from './DoneItem.scss';

class DoneItem extends Component {

    constructor(props) {
        super(props);
        
        this.deleteMe = this.deleteMe.bind(this);
        this.moveToNotDone = this.moveToNotDone.bind(this);
    }

    deleteMe() {
        this.props.deleteMe(this.props.items.key);
    }

    moveToNotDone() {
        this.props.moveToNotDone(this.props.items.key);
    }

    render() {

        const item = this.props.items;

        if (!item.done) { return false; }

        return (
            <div className="done-item_container">

                <p>{item.text}</p>

                <span onClick={this.deleteMe}>X</span>

                <span onClick={this.moveToNotDone}>@</span>

            </div>
        );

    }
};

export default DoneItem;
